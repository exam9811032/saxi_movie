package com.saxipapsi.saxi_movie.domain.use_case

import android.util.Log
import com.saxipapsi.saxi_movie.common.Resource
import com.saxipapsi.saxi_movie.data.remote.dto.ContentDetailsDto
import com.saxipapsi.saxi_movie.domain.model.ContentModel
import com.saxipapsi.saxi_movie.domain.repository.TMDBRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GetContentDetailsUseCase (private val tmdbRepository: TMDBRepository) {
    operator fun invoke(movieId : Int): Flow<Resource<ContentDetailsDto>> = flow {
        try {
            Log.d("eric", "LoadGeoLocationUseCase")
            emit(Resource.Loading())
            val data = tmdbRepository.getContentDetails(movieId)
            emit(Resource.Success(data))
        } catch (e: HttpException) {
            emit(Resource.Error(e.localizedMessage ?: "An unexpected error occured."))
        } catch (e: IOException) {
            emit(Resource.Error("Couldn't reach server. Please check your internet connection"))
        }
    }
}