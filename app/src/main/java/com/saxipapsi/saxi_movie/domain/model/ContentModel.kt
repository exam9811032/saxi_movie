package com.saxipapsi.saxi_movie.domain.model

data class ContentModel(val id : Int, val title: String? = null, val banner: String? = null)
