package com.saxipapsi.saxi_movie.domain.repository

import com.saxipapsi.saxi_movie.data.remote.dto.ContentDetailsDto
import com.saxipapsi.saxi_movie.data.remote.dto.ContentPageDto

interface TMDBRepository {
    suspend fun getContentList(): ContentPageDto
    suspend fun getContentDetails(movieId: Int): ContentDetailsDto
}