package com.saxipapsi.saxi_movie.common


object API {
    object TMDBApi {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val MOVIE = "movie/"
        const val MOVIE_POPULAR = "$MOVIE/popular"
    }
}
