package com.saxipapsi.saxi_movie.data.remote.dto

import com.saxipapsi.saxi_movie.domain.model.ContentModel

data class ContentDto(
    val adult: Boolean,
    val backdrop_path: String,
    val genre_ids: List<Int>,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
){
    fun toContentModel() : ContentModel = ContentModel(id, original_title, poster_path)

}