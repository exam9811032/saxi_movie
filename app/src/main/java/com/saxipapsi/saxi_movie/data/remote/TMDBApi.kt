package com.saxipapsi.saxi_movie.data.remote

import com.saxipapsi.saxi_movie.common.API
import com.saxipapsi.saxi_movie.data.remote.dto.ContentDetailsDto
import com.saxipapsi.saxi_movie.data.remote.dto.ContentPageDto
import retrofit2.http.*

interface TMDBApi {
    /*TODO Token should be protected*/
    @Headers(
        "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ZjQxMDBkZTE4NDNjNGI0MDU1NWYxYTAxNTlkNDI3NiIsInN1YiI6IjY0ZDYwNmUyZjE0ZGFkMDBhZDRlM2UzMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.MJHaw0MmmcOlW1hbLacBsF1FBXagszr-RQfYQW3gYZ8",
        "Content-Type: application/json"
    )
    @GET(API.TMDBApi.MOVIE_POPULAR)
    suspend fun getContentList(@Query("api_key") api_key: String = "7f4100de1843c4b40555f1a0159d4276"): ContentPageDto


    /*TODO Token should be protected*/
    @Headers(
        "Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3ZjQxMDBkZTE4NDNjNGI0MDU1NWYxYTAxNTlkNDI3NiIsInN1YiI6IjY0ZDYwNmUyZjE0ZGFkMDBhZDRlM2UzMiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.MJHaw0MmmcOlW1hbLacBsF1FBXagszr-RQfYQW3gYZ8",
        "Content-Type: application/json"
    )
    @GET("${API.TMDBApi.MOVIE}{movieId}")
    suspend fun getContentDetails(@Path("movieId") movieId : Int, @Query("api_key") api_key: String = "7f4100de1843c4b40555f1a0159d4276"): ContentDetailsDto


}