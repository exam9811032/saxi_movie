package com.saxipapsi.saxi_movie.data.repository

import com.saxipapsi.saxi_movie.data.remote.TMDBApi
import com.saxipapsi.saxi_movie.data.remote.dto.ContentDetailsDto
import com.saxipapsi.saxi_movie.data.remote.dto.ContentPageDto
import com.saxipapsi.saxi_movie.domain.repository.TMDBRepository

class TMDBRepositoryImpl(private val tmdbApi: TMDBApi) : TMDBRepository {
    override suspend fun getContentList(): ContentPageDto = tmdbApi.getContentList()
    override suspend fun getContentDetails(movieId: Int): ContentDetailsDto =
        tmdbApi.getContentDetails(movieId = movieId)
}