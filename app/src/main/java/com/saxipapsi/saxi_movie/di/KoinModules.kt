package com.saxipapsi.saxi_movie.di

import android.app.Application
import com.saxipapsi.saxi_movie.data.repository.TMDBRepositoryImpl
import com.saxipapsi.saxi_movie.domain.repository.TMDBRepository
import com.saxipapsi.saxi_movie.domain.use_case.GetContentListUseCase
import com.saxipapsi.saxi_movie.presentation.get_content_list.components.GetContentListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import com.saxipapsi.saxi_movie.data.remote.networkModule
import com.saxipapsi.saxi_movie.domain.use_case.GetContentDetailsUseCase
import com.saxipapsi.saxi_movie.presentation.get_content_details.components.GetContentDetailsViewModel

object KoinModules {

    fun init(app: Application) {
        startKoin() {
            androidLogger(Level.ERROR)
            androidContext(app)
            modules(
                networkModule,
                repositoryModule,
                useCasesModule,
                viewModelModule
            )
        }
    }
}


val repositoryModule = module {
    single { TMDBRepositoryImpl(get()) as TMDBRepository }
}

val useCasesModule = module {
    single { GetContentListUseCase(get()) }
    single { GetContentDetailsUseCase(get()) }
}

val viewModelModule = module {
    viewModel { GetContentListViewModel(get()) }
    viewModel { GetContentDetailsViewModel(get()) }
}
