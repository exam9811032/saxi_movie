package com.saxipapsi.saxi_movie.presentation.get_content_list.components

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.saxipapsi.saxi_movie.R
import com.saxipapsi.saxi_movie.domain.model.ContentModel
import com.saxipapsi.saxi_movie.presentation.get_content_details.components.ContentDetailsActivity

class ContentListAdapter() : RecyclerView.Adapter<ContentListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContentListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_content_list_item, parent, false)
        return ContentListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContentListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size

    private val diffUtil = object : DiffUtil.ItemCallback<ContentModel>() {
        override fun areItemsTheSame(oldItem: ContentModel, newItem: ContentModel): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: ContentModel, newItem: ContentModel): Boolean =
            oldItem == newItem
    }
    val differ = AsyncListDiffer(this, diffUtil)
}

class ContentListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val ivBanner: ImageView = itemView.findViewById(R.id.ivBanner)
    private val tvBanner: TextView = itemView.findViewById(R.id.tvBanner)
    fun bind(contentModel: ContentModel) {
        val fullPath = "https://image.tmdb.org/t/p/original${contentModel.banner}"
        Glide.with(itemView.context).load(fullPath).dontAnimate().into(ivBanner)
        tvBanner.text = contentModel.title
        itemView.setOnClickListener { ContentDetailsActivity.start(itemView.context, contentModel.id) }
    }


}