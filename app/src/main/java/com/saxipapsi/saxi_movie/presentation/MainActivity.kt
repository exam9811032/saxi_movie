package com.saxipapsi.saxi_movie.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.saxipapsi.saxi_movie.databinding.ActivityMainBinding
import com.saxipapsi.saxi_movie.presentation.get_content_list.components.ContentListAdapter
import com.saxipapsi.saxi_movie.presentation.get_content_list.components.GetContentListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val getContentListViewModel: GetContentListViewModel by inject()
    private val contentListAdapter: ContentListAdapter by lazy { ContentListAdapter() }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rvContentList.adapter = contentListAdapter
        initObserver()
    }

    private fun initObserver(){
        lifecycleScope.launch { observeContentList() }
    }

    private suspend fun observeContentList(){
        getContentListViewModel.state.collectLatest { state ->
            binding.loading.visibility = if (state.isLoading) View.VISIBLE else View.GONE
            binding.tvError.visibility = if (state.error.isNotEmpty()) View.VISIBLE else View.GONE
            binding.tvError.text = state.error
            contentListAdapter.differ.submitList(state.data)
        }
    }
}