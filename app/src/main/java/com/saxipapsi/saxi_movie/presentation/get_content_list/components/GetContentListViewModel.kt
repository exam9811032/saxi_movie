package com.saxipapsi.saxi_movie.presentation.get_content_list.components

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saxipapsi.saxi_movie.common.Resource
import com.saxipapsi.saxi_movie.domain.model.ContentModel
import com.saxipapsi.saxi_movie.domain.use_case.GetContentListUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class GetContentListViewModel(private val getContentListUseCase: GetContentListUseCase) : ViewModel() {
    private val _state = MutableStateFlow(ContentListState())
    val state : StateFlow<ContentListState> = _state.apply { getContentList() }
    private fun getContentList(){
        getContentListUseCase().onEach { resource ->
            when(resource){
                is Resource.Loading -> _state.value = ContentListState(isLoading = true)
                is Resource.Error -> _state.value = ContentListState(error = resource.message ?: "Unexpected error occured.")
                is Resource.Success -> _state.value = ContentListState(data = resource.data)
            }
        }.launchIn(viewModelScope)
    }
}

data class ContentListState(
    val isLoading: Boolean = false,
    val data: List<ContentModel>? = null,
    val error: String = ""
)