package com.saxipapsi.saxi_movie.presentation.get_content_details.components

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.saxipapsi.saxi_movie.R
import com.saxipapsi.saxi_movie.databinding.ActivityContentDetailsBinding
import com.saxipapsi.saxi_movie.databinding.ActivityMainBinding
import com.saxipapsi.saxi_movie.presentation.get_content_list.components.GetContentListViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ContentDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityContentDetailsBinding
    private val getContentDetailsViewModel: GetContentDetailsViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityContentDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObserver()
        val id = intent.getIntExtra("movieId", 0)
        getContentDetailsViewModel.getContentDetails(id)
    }

    private fun initObserver() {
        lifecycleScope.launch { observeContentDetails() }
    }

    private suspend fun observeContentDetails() {
        getContentDetailsViewModel.state.collectLatest { state ->
            binding.loading.visibility = if (state.isLoading) View.VISIBLE else View.GONE
            binding.tvError.visibility = if (state.error.isNotEmpty()) View.VISIBLE else View.GONE
            binding.tvError.text = state.error
            val data = state.data
            val fullPath = "https://image.tmdb.org/t/p/original${data?.poster_path}"
            Glide.with(this).load(fullPath).dontAnimate().into(binding.ivBanner)
            binding.tvBanner.text = data?.title
        }
    }

    companion object {
        fun start(context: Context, movieId: Int) {
            val intent = Intent(context, ContentDetailsActivity::class.java)
            intent.putExtra("movieId", movieId)
            context.startActivity(intent)
        }
    }
}